/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public AudioIODeviceCallback,
                        public Slider::Listener,
                        public MidiInputCallback
{
public:
    //==============================================================================
    /** Constructor */
    MainComponent ();

    /** Destructor */
    ~MainComponent();

    void resized() override;
    

    void audioDeviceIOCallback (const float **inputChannelData, int numInputChannels, float **outputChannelData, int numOutputChannels, int numSamples) override;
    void audioDeviceAboutToStart(AudioIODevice *device) override;
    void audioDeviceStopped() override;
    void sliderValueChanged (Slider* slider) override;
    
    void handleIncomingMidiMessage (MidiInput* midiInput, const MidiMessage& message) override;
    
    
private:

    AudioDeviceManager audioDeviceManager;
    Slider amplitudeSlider;
    float amplitude;
    Label midiLabel;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
