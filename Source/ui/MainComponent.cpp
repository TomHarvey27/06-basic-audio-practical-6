/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent ()
{
    setSize (500, 400);
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addAudioCallback (this);
    
    amplitudeSlider.setRange(0.0, 1.0);
    addAndMakeVisible(amplitudeSlider);
    amplitudeSlider.addListener(this);

    amplitude = 0;
    
    audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true);     // Sets the input from a keyboard
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    
    
 
}

MainComponent::~MainComponent()
{
    
    audioDeviceManager.removeAudioCallback (this);

}

void MainComponent::resized()
{
    
    amplitudeSlider.setBounds(0, 30, getWidth(), 30);
    
}

void MainComponent::audioDeviceIOCallback (const float **inputChannelData, int numInputChannels, float **outputChannelData, int numOutputChannels, int numSamples)
{
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        *outL = *inL * amplitude;
        *outR = *inR * amplitude;
        inL++;
        inR++;
        outL++;
        outR++;
    }
    
    //DBG("Callback");
}
void MainComponent::audioDeviceAboutToStart(AudioIODevice *device)
{
    DBG("About to start");
}
void MainComponent::audioDeviceStopped()
{
    
    DBG("Stopped");
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    if(slider == &amplitudeSlider)
    {
        
        amplitude = slider -> getValue();
    }

    std::cout << amplitude << "\n";
}



void MainComponent::handleIncomingMidiMessage(MidiInput* midiInput, const MidiMessage& message)
{
    //DBG("MidiMessage");
    
    
    String midiText;
    
    if (message.isController())
    {
        if (message.getControllerNumber() == 1)
        {
            
            amplitude = message.getControllerValue()/127.0;
            DBG("Modwhell");
        }
     
        
        
    }
    
    
    if (message.isNoteOnOrOff()) {
        midiText << "NoteOn: Channel " << message.getChannel(); midiText << ":Number" << message.getNoteNumber(); midiText << ":Velocity" << message.getVelocity();
    }
    
    midiLabel.getTextValue() = midiText;
    
    // this function basically prints the input of the virtual synth to text in the window
    
}
